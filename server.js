const { ApolloServer } = require("apollo-server");
const axios = require("axios");

const photoData = async (count = 8) => {
  try {
    const response = await axios.get(
      `https://randomuser.me/api/?results=${count}`
    );
    const userData = await response.data;
    const photosResult = [];
    let i = 1;
    for (const {
      picture: { thumbnail: image },
    } of userData.results) {
      photosResult.push({
        id: i++,
        image,
      });
    }
    return photosResult;
  } catch (err) {
    console.error(err);
  }
};

const typeDefs = `
    type Photo{
        id: ID
        image: String
    }
    type Query {
        photos: [Photo!]!
    }
    
`;

const resolvers = {
  Query: {
    photos: () => photoData(),
  },
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
});

server
  .listen({
    port: 3124,
  })
  .then((res) => {
    console.log(`The server is up at ${res.url}`);
  })
  .catch((err) => {
    console.log(err);
  });
